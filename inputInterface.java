package Assignment7;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

// handles things relating to the correct color pattern
/////////////////////////////
public class inputInterface {

	private int[] correctNumOrder = new int[4]; //stores the correct number sequence
	int numGameColors;
	int numBoardPegs;
	
	// simple translator of base number to base color
	////////////////////////////
	String numToCol( int num ) {
		
		switch( num ) {
		case 0: return "R";
		case 1: return "G";
		case 2: return "B";
		case 3: return "Y";
		default: return "error";
		}
	}
	
	// simple constructor that records basic data about the game for this class
	// prints greeting message, generates pattern
	/////////////////////////////////////////////////////
	inputInterface( int numGameColors, int numBoardPegs )
	{
		this.numGameColors = numGameColors;
		this.numBoardPegs  = numBoardPegs;
		
		for( int z = 0; z < 4; z++ ) {
			correctNumOrder[z] = randomNumberGenerator( numGameColors );
		}
		
		System.out.println("Welcome to Mastermind. Here are the rules. \n This is a text version of the classic board game Mastermind. "
				+ "\n The computer will think of a secret code. The code consists of 4 colored pegs. \n "
				+ "The pegs may be one of six colors: blue, green, orange, purple, red, or yellow. A color may appear \n "
				+ "more than once in the code. You try to guess what colored pegs are in the code and what order they are \n "
				+ "in. After you make a valid guess the result (feedback) will be displayed. \n "
				+ "The result consists of a black peg for each peg you have guessed exactly correct (color and position) in \n "
				+ "your guess. For each peg in the guess that is the correct color, but is out of position, you get a white \n "
				+ "peg. For each peg, which is fully incorrect, you get no feedback. \n "
				+ "When entering guesses you only need to click on any pre-colored bottom peg and then the desired peg. \n "
				+ "You have 12 guesses to figure out the secret code or you lose the game.");
		
		
		System.out.println("The correct Master-Mind color sequence is " + this.numToCol(correctNumOrder[0])
		+ this.numToCol(correctNumOrder[1]) + this.numToCol(correctNumOrder[2]) + this.numToCol(correctNumOrder[3]));
	}
	
	// gnerates random number for pattern generator
	/////////////////////////////////////////////////////
	public int randomNumberGenerator( int numGameColors )
	{
		int randInt = 0;
		//get a random number to generate a random combination of colors
		Random tempNum = new Random();
		
		return tempNum.nextInt( numGameColors );//gets a number between 0 and numGameColors
	}
	
	// processes guess, returns number of black and white pegs
	// via helper class BlackWhiteResults
	///////////////////////////////////////////////////////////
	public BlackWhiteResults checkCombinationGuess( int[] inputNumbers )
	{
		int correctBlack   = 0;    //stores the number of correct pegs (same color and spot)
		int correctWhite   = 0;    //stores the pegs that are the same color, but not same spot
		boolean whiteFound = false;
		String finalString = "";
		int[] tempCorrectCombo = new int[4];
		
		//get the correct combo into a disposable array
		for( int z=0; z < 4; z++ ) {
		
			tempCorrectCombo[z] = correctNumOrder[z];
		}
		
		//finds how many are the same peg in the same spot, if so it is a black peg
		for( int z = 0; z < 4; z++ )
		{
			if( inputNumbers[z] == tempCorrectCombo[z] )
			{
				correctBlack++;
				inputNumbers[z] = -1;
				tempCorrectCombo[z] = -1;
			}	
		}
		
		//finds how many white pegs there are (same color, but wrong spot)
		for( int z = 0; z < 4; z++ )
		{
			for( int q = 0; q < 4 && whiteFound == false; q++ )
			{	
				if( ( inputNumbers[z] == tempCorrectCombo[q] ) && ( tempCorrectCombo[q] != -1 ) )
				{
					correctWhite++;
					tempCorrectCombo[q] = -1;
					whiteFound          = true;
				}
			}
			whiteFound=false;
		}

		if( correctBlack == this.numBoardPegs ) {
			return new BlackWhiteResults( 4, 0, true );
		}
		
		return new BlackWhiteResults( correctBlack, correctWhite, false );
	}
}
