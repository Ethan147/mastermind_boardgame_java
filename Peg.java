package Assignment7;

import java.awt.Color;

// helper class for general pegs
/////////////////////
public class Peg {
	
	int   row;
	int   column;
	Color color;
	int   pegNum;
	
	// one of two constructors with default color
	///////////////////////////////////
	public Peg( int row, int column ) {
		this.row    = row;
		this.column = column;
		color 		= Color.PINK;
	}
	
	// second, more complete constructor
	////////////////////////////////////////////////////////////
	public Peg( int row, int column, Color color, int pegNum ) {
		this.row    = row;
		this.column = column;
		this.color  = color;		
		this.pegNum = pegNum;
	}
	
	// getter for the peg number
	/////////////////////////
	public int getPegNum( ) {
		return this.pegNum;
	}
	
	// getter for the row number
	//////////////////////
	public int getRow( ) {
		return this.row;
	}
	
	// getter for the column
	/////////////////////////
	public int getColumn( ) {
		return this.column;
	}
	
	/// getter for the color
	//////////////////////////
	public Color getColor( ) {
		return this.color;
	}

}
