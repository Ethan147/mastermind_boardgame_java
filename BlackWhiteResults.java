package Assignment7;

// holds black/white result peg data
/////////////////////////
class BlackWhiteResults {
	
	int bl, wh;
	boolean correctGuess = false;
	
	// constructor with the number of black and white results as well as winning conditions
	/////////////////////////////////////////////////////////////////
	BlackWhiteResults( int black, int white, boolean patternMatch ) {
		this.bl           = black;
		this.wh           = white;
		this.correctGuess = patternMatch;
	}
	
	// returns num black results
	////////////////////////
	int getBlackResults( ) {
		return this.bl;
	}
	
	// returns num white results
	////////////////////////
	int getWhiteResults( ) {
		return this.wh;
	}
	
	// returns if all patterns have been matched
	///////////////////////////
	boolean patternMatched( ) {
		return this.correctGuess;
	}
}
