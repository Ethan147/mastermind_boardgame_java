package Assignment7;

import java.awt.Color;

// simple helper class for black/white resulte pegs
////////////////////////
public class ResultPeg {
	
	int   pegRow;
	int   pegNum; // of 4 possible
	Color pegColor;
	
	// constructor including row and which of the four pegs is intended
	///////////////////////////////////////////////////
	public ResultPeg( int row, int num, Color color ) {
		this.pegColor = color;
		this.pegNum   = num;
		this.pegRow   = row;
	}
	
	// getter for the row number
	////////////////////////////
	public int getResultRow( ) {
		return this.pegRow;
	}
	
	// getter for the peg number of the four
	////////////////////////////
	public int getResultNum( ) {
		return this.pegNum;
	}
	
	// getter for the result color
	////////////////////////////////
	public Color getResultColor( ) {
		return this.pegColor;
	}
}