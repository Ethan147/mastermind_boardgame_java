package Assignment7;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.net.*;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import java.io.*;

// implements the bulk of game infrastructure
///////////////////////////////////////////////////////////////////////////////
public class MouseInterface extends Applet implements MouseListener, Runnable {

	private int     xCoord       = 0; // Stores the x coordinate of button push
	private int     yCoord       = 0; // Stores the y coordinate of button push
	
	int stageWidth  = 400;
	int stageHeight = 425;
	
    Image    bufImg;
    Graphics bufG;
    
    ///////////////////////
    public enum gameBoard {
    	eightRows
    }
    
    gameBoard gameSetting;
    
    // basic peg definitions
    ////////////////////
    static int maxNumPegs = 20;
    static int pegX = 0;
    static int pegY = 1;
    int       numBasePegs;
    int[][]   basePegLocations     = new int  [maxNumPegs][2]; // [peg#][x or y]
    Color[]   basePegColors        = new Color[maxNumPegs];
    int[][][] boardPegLocations    = new int  [maxNumPegs][maxNumPegs][2]; // [peg row][peg column][x or y]
    Peg[]     pegsToPrint		   = new Peg  [50];
    int[][][] resultPegLocations   = new int [maxNumPegs][4][2]; // [peg row][peg number][x or y]
    ResultPeg[] resultPegsToPrint  = new ResultPeg [50];
    int       resultPegRadius;
    int       pegRadius;
    int 	  boardPegColumns;
    int 	  boardPegRows;
    Peg		  toPaint;
    boolean   gameStarted = false;
    boolean   gameOver    = false;
    boolean   gameWon     = false;
    int       boardCurrentRow     = 0;
    int[]     boardCurrentColumns = new int[4];
    int       cursorBasePegNum    = -1;
    
    //
    Cursor cursor = Cursor.getDefaultCursor();
    Color  cursorColor;
        
    // 
    inputInterface game = null; 
    
    // initializes peg locations
    /////////////////////////////
    void initPegs( Graphics g ) {
    	
    	switch( this.gameSetting ) {
 
    	case eightRows: 
    		// basePegs
    		this.basePegLocations[0][pegX] = 199;
    		this.basePegLocations[0][pegY] = 530;
    		this.basePegColors[0]          = Color.RED;
			
			this.basePegLocations[1][pegX] = 235;
    		this.basePegLocations[1][pegY] = 530;
    		this.basePegColors[1]          = Color.GREEN;
    		
			this.basePegLocations[2][pegX] = 269;
    		this.basePegLocations[2][pegY] = 530;
    		this.basePegColors[2]          = Color.BLUE;
    		
			this.basePegLocations[3][pegX] = 305;
    		this.basePegLocations[3][pegY] = 530;
    		this.basePegColors[3]          = Color.YELLOW;
    		
			this.numBasePegs = 4;
			this.pegRadius = 20;
			
			// base Peg fillings
			Color pinCol = this.basePegColors[0];
			int   xLoc   = this.basePegLocations[0][pegX] - this.pegRadius/2 + 1;
			int   yLoc   = this.basePegLocations[0][pegY] - this.pegRadius/2 + 1;
			bufG.setColor( pinCol );
			bufG.fillOval( xLoc, yLoc, this.pegRadius, this.pegRadius);
			
			pinCol = this.basePegColors[1];
			xLoc   = this.basePegLocations[1][pegX] - this.pegRadius/2 + 1;
			yLoc   = this.basePegLocations[1][pegY] - this.pegRadius/2 + 1;
			bufG.setColor( pinCol );
			bufG.fillOval( xLoc, yLoc, this.pegRadius, this.pegRadius);
			
			pinCol = this.basePegColors[2];
			xLoc   = this.basePegLocations[2][pegX] - this.pegRadius/2 + 1;
			yLoc   = this.basePegLocations[2][pegY] - this.pegRadius/2 + 1;
			bufG.setColor( pinCol );
			bufG.fillOval( xLoc, yLoc, this.pegRadius, this.pegRadius);
			
			pinCol = this.basePegColors[3];
			xLoc   = this.basePegLocations[3][pegX] - this.pegRadius/2 + 1;
			yLoc   = this.basePegLocations[3][pegY] - this.pegRadius/2 + 1;
			bufG.setColor( pinCol );
			bufG.fillOval( xLoc, yLoc, this.pegRadius, this.pegRadius);
			
			// board Peg locations 
			int pegXoffset = 35;
			int pegYoffset = 40;
			int startX = 200;
			int startY = 480;
			int xVal = startX;
			int yVal = startY;
			int discontOffset = 0;
			
			
			this.boardPegRows    = 12;
			this.boardPegColumns = 4;
			
			for( int y = 0; y < this.boardPegRows; y++ ) {
				for( int x = 0; x < this.boardPegColumns; x++ ) {
					
					if( x == 0 ) { 
						xVal = startX; 
					}
					
					if( y == 4 ) {
						discontOffset = 4;
					}
					
					this.boardPegLocations[ y ][ x ][pegX] = xVal; 
					this.boardPegLocations[ y ][ x ][pegY] = yVal + discontOffset;
					
//				    int[][][] boardPegLocations    = new int  [maxNumPegs][maxNumPegs][2]; // [peg row][peg column][x or y]
					
//					xLoc = this.boardPegLocations[y][x][pegX] - this.pegRadius/2 + 1;
//					yLoc = this.boardPegLocations[y][x][pegY] - this.pegRadius/2 + 1;
					
//					bufG.setColor( Color.PINK );
//					bufG.fillOval( xLoc, yLoc, this.pegRadius, this.pegRadius);
					
					xVal += pegXoffset;
				}
				yVal -= pegYoffset;
			}
			
			game                        = new inputInterface( this.numBasePegs, this.boardPegColumns );
			
			/// take care of result pegs
			Color pegColor = Color.GRAY;
			this.resultPegRadius = 10;
			
			// [peg row][peg number][x or y]
			
			int pegXstart = 340;
			int pegYstart = 472;
			
			int currentX = pegXstart;
			int currentY = pegYstart;
			
			int pegYdelta = 40;
			
			discontOffset = 0;
			
			bufG.setColor( Color.gray );
			
			for( int y = 0; y < this.boardPegRows; y++ ) {

				if( y == 4 ) {
					discontOffset = 5;
				}
				
				this.resultPegLocations[y][0][pegX] = currentX;
				this.resultPegLocations[y][0][pegY] = currentY + discontOffset;
				bufG.fillOval( this.resultPegLocations[y][0][pegX] - this.resultPegRadius/2 + 1, 
							   this.resultPegLocations[y][0][pegY] - this.resultPegRadius/2 + 1, 
							   this.resultPegRadius, 
							   this.resultPegRadius );

				this.resultPegLocations[y][1][pegX] = currentX + 14;
				this.resultPegLocations[y][1][pegY] = currentY + discontOffset;
				bufG.fillOval( this.resultPegLocations[y][1][pegX] - this.resultPegRadius/2 + 1, 
							   this.resultPegLocations[y][1][pegY] - this.resultPegRadius/2 + 1, 
							   this.resultPegRadius, 
							   this.resultPegRadius );
				
				this.resultPegLocations[y][2][pegX] = currentX;
				this.resultPegLocations[y][2][pegY] = currentY + 14 + discontOffset;
				bufG.fillOval( this.resultPegLocations[y][2][pegX] - this.resultPegRadius/2 + 1, 
							   this.resultPegLocations[y][2][pegY] - this.resultPegRadius/2 + 1, 
							   this.resultPegRadius, 
							   this.resultPegRadius );
				
				this.resultPegLocations[y][3][pegX] = currentX + 14;
				this.resultPegLocations[y][3][pegY] = currentY + 14 + discontOffset;
				bufG.fillOval( this.resultPegLocations[y][3][pegX] - this.resultPegRadius/2 + 1, 
							   this.resultPegLocations[y][3][pegY] - this.resultPegRadius/2 + 1, 
							   this.resultPegRadius, 
							   this.resultPegRadius );
				
				currentY -= pegYdelta;
			}
			
			for( int i = 0; i < this.resultPegsToPrint.length; i++ ) {
				
				this.resultPegsToPrint[i] = new ResultPeg( -1, -1, Color.PINK );
			}
			
			break;
			
    	default: break;
    	}
    }
    
    // simple super constructor
	/////////////////////////
	public MouseInterface() {
		
		super();
	}
	
	// unimplemented run function
	/////////////////////
	public void run( ) {

	}

	// initializes game variables
	//////////////////////
	public void init ( ) {

		this.boardCurrentRow        = 0;
		this.boardCurrentColumns[0] = -1;
		this.boardCurrentColumns[1] = -1;
		this.boardCurrentColumns[2] = -1;
		this.boardCurrentColumns[3] = -1;
		
		gameSetting = gameBoard.eightRows;
		
	    bufImg	    = createImage( this.stageWidth, this.stageHeight );

	    try {
	           URL pic = new URL(getDocumentBase(), "Mastermind_Board.png");
	           bufImg  = ImageIO.read(pic);
	        } catch(Exception e) {

	           e.printStackTrace();
	        }
		bufG = bufImg.getGraphics();
		
		for( int z = 0; z < this.pegsToPrint.length; z++ ) {
		
			this.pegsToPrint[z] = new Peg(-1,-1);
		}
		
		this.addMouseListener(this);	
    }

	// paints pegs and results
	/////////////////////////////////
	public void paint( Graphics g ) {
		
		if( this.gameStarted == false ) {
			this.initPegs(g);
			this.gameStarted = true;
		}
	    
		for( int z = 0; z < this.pegsToPrint.length; z++ ) {
			
			if( this.pegsToPrint[z].getColumn() != -1 ) {
				
				Color pegColor = this.pegsToPrint[z].getColor();
				int   column   = this.pegsToPrint[z].getColumn();
				int   row      = this.pegsToPrint[z].getRow();
				int   xLoc     = this.boardPegLocations[row][column][pegX] - this.pegRadius/2 + 1;
				int   yLoc     = this.boardPegLocations[row][column][pegY] - this.pegRadius/2 + 1;

				bufG.setColor( pegColor );
				bufG.fillOval( xLoc, yLoc, this.pegRadius, this.pegRadius );
			}
		}
		
		for( int q = 0; q < this.resultPegsToPrint.length; q++ ) {

			if( this.resultPegsToPrint[q].getResultNum() != -1 ) {
				Color pegColor  = this.resultPegsToPrint[q].getResultColor();
				int   pegNumber = this.resultPegsToPrint[q].getResultNum();
				int   pegRow    = this.resultPegsToPrint[q].getResultRow();
				
	////		    int[][][] resultPegLocations = new int [maxNumPegs][4][2]; // [peg row][peg number][x or y]
				
				int   xLocation = this.resultPegLocations[ pegRow ][ pegNumber ][ pegX ] - this.resultPegRadius/2 + 1;
				int   yLocation = this.resultPegLocations[ pegRow ][ pegNumber ][ pegY ] - this.resultPegRadius/2 + 1;
				
	////			int   xLoc     = this.boardPegLocations[row][column][pegX] - this.pegRadius/2 + 1;
	////			int   yLoc     = this.boardPegLocations[row][column][pegY] - this.pegRadius/2 + 1;
	
				bufG.setColor( pegColor );
				bufG.fillOval( xLocation, yLocation, this.resultPegRadius, this.resultPegRadius );
			}
		}
		
		g.drawImage( bufImg, 0, 0, this );
	}

	// returns numerical key of latest base peg click
	/////////////////////////////////////////////////////////
	public int mouseBasePegSelect( int xClick, int yClick ) {
		
		for( int z = 0; z < this.numBasePegs; z++ ) {
			
			if( (xClick <= this.basePegLocations[z][pegX] + this.pegRadius/2) && (xClick >= this.basePegLocations[z][pegX] - this.pegRadius/2)
			 && (yClick <= this.basePegLocations[z][pegY] + this.pegRadius/2) && (yClick >= this.basePegLocations[z][pegY] - this.pegRadius/2) ) {
			
				return z;
			}
		}
		
		return -1;
	}
	
	// returns numerical key of latest board peg click
	//////////////////////////////////////////////////////////
	public Peg mouseBoardPegSelect( int xClick, int yClick ) {
	
		for( int y = 0; y < this.boardPegRows; y++ ) {
			for( int x = 0; x < this.boardPegColumns; x++ ) {
				
				if( (xClick <= this.boardPegLocations[y][x][pegX] + this.pegRadius/2) && (xClick >= this.boardPegLocations[y][x][pegX] - this.pegRadius/2)
				&&  (yClick <= this.boardPegLocations[y][x][pegY] + this.pegRadius/2) && (yClick >= this.boardPegLocations[y][x][pegY] - this.pegRadius/2)) {
					
					return new Peg( y, x );
				}
			}
		}
		
		// none found
		return new Peg( -1, -1 );
	}
	
	// bulk of the work done here
	// tells what pegs to print etc
	//////////////////////////////////////////
	public void mousePressed( MouseEvent e ) {

		xCoord = e.getX();
		yCoord = e.getY();

		int basePegClicked  = this.mouseBasePegSelect(  xCoord, yCoord );
		Peg boardPegClicked = this.mouseBoardPegSelect( xCoord, yCoord );
				
		if( this.gameOver == true ) {
			
			return;
		}
		
		if( basePegClicked != -1 ) {

			int cursRadius = 10;		
			Toolkit kit = Toolkit.getDefaultToolkit();
		    BufferedImage buffered = new BufferedImage( cursRadius, cursRadius, BufferedImage.BITMASK );
		    Graphics2D gg = buffered.createGraphics();
			
			switch( basePegClicked ) {
			case 0: cursorColor = this.basePegColors[0]; gg.setColor( cursorColor ); this.cursorBasePegNum = 0; break;
			case 1: cursorColor = this.basePegColors[1]; gg.setColor( cursorColor ); this.cursorBasePegNum = 1; break;
			case 2: cursorColor = this.basePegColors[2]; gg.setColor( cursorColor ); this.cursorBasePegNum = 2; break;
			case 3: cursorColor = this.basePegColors[3]; gg.setColor( cursorColor ); this.cursorBasePegNum = 3; break;
			case 4: 
			case 5:
			case 6:
			case 7: 
			case 8: 
			default: System.out.println("peg clicking error");
			}
			
			gg.fillOval(0, 0, cursRadius, cursRadius );
		    int centerX = cursRadius/2;
		    int centerY = cursRadius/2;
		    gg.dispose();
		    cursor = kit.createCustomCursor(buffered, new Point(centerX, centerY), "myCursor");
		    setCursor(cursor);
		}
		
		// deal with board peg click
		int cursorType = cursor.getType();
		
		if( (boardPegClicked.getRow() == this.boardCurrentRow) && (cursor.getType() != Cursor.DEFAULT_CURSOR) 
	     && (this.boardCurrentColumns[boardPegClicked.getColumn()] == -1) ) {
			
			this.boardCurrentColumns[ boardPegClicked.getColumn() ] = this.cursorBasePegNum;
			
			for( int z = 0; z < this.boardPegColumns; z++ ) {
				
				if( this.boardCurrentColumns[z] == -1 ) { 
					break; 
				}
				
				if( z == this.boardPegColumns - 1 ) {
					
					if( this.boardCurrentRow == this.boardPegRows - 1 ) {
						System.out.println("The game is over and you have lost.");
						this.gameOver = true;
					}

					BlackWhiteResults results = game.checkCombinationGuess( this.boardCurrentColumns );
					
					int numBlack = results.getBlackResults();
					int numWhite = results.getWhiteResults();
					
					for( int l = 0; l < 4; l++ ) {
						for( int q = 0; q < this.resultPegsToPrint.length; q++ ) {
							
							if( resultPegsToPrint[q].getResultNum() == -1 ) {
								
								if( numBlack > 0 ) {
									resultPegsToPrint[q] = new ResultPeg( this.boardCurrentRow, l, Color.BLACK );
									numBlack--;
									break;
								}
								
								if( numWhite > 0 ) {
									resultPegsToPrint[q] = new ResultPeg( this.boardCurrentRow, l, Color.WHITE );
									numWhite--;
									break;
								}
								
								if( (numWhite == 0) && (numBlack==0) ) {
									break;
								}
							}
						}
					}
					
						if( results.patternMatched() ) {
							this.gameOver = true;
							this.gameWon  = true;
							System.out.println("The game is over, congraduations you have won!");
						}
						
						this.boardCurrentColumns[0] = -1;
						this.boardCurrentColumns[1] = -1;
						this.boardCurrentColumns[2] = -1;
						this.boardCurrentColumns[3] = -1;
						
						this.boardCurrentRow++;					
				}
			}
			
			int y = boardPegClicked.getRow();
			int x = boardPegClicked.getColumn();
			
			for( int z = 0; z < this.pegsToPrint.length; z++ ) {
				
				if( this.pegsToPrint[z].getColumn() == -1 ) {
					
					this.pegsToPrint[z] = new Peg( y, x, cursorColor, this.cursorBasePegNum );
					break;
				}
			}
			
			this.cursor = Cursor.getDefaultCursor();
			setCursor( this.cursor );
		}
		
		repaint();
	}

	// unimplemented mouse listening functions
	/////////////////////////////////////////////
	public void mouseReleased ( MouseEvent e ) {}
	public void mouseEntered  ( MouseEvent e ) {}
	public void mouseExited   ( MouseEvent e ) {}
	public void mouseClicked  ( MouseEvent e ) {}
	
	// simply calls paint
	//////////////////////////////////
	public void update( Graphics g ) {
		
		paint(g);
	}
	
	// tells latest click x
	////////////////////////
	public int getXCoord() {
		
		return xCoord;
	}
	
	// tells latest click y
	////////////////////////
	public int getYCoord() {
		
		return yCoord;
	}
		
}
